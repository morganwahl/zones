#!/usr/bin/env python3

# https://api.weather.com
# /v2/pws/history/
# hourly?
#   stationId=KMAARLIN42
#   &format=json
#   &units=e
#   &startDate=20200701
#   &endDate=20200731
#   &numericPrecision=decimal
#   &apiKey=6532d6454b8aa370768e63d6ba5a832e

import datetime
import json
import sys
from urllib.parse import urlencode

import requests

BASE_URL = 'https://api.weather.com/v2/pws/history/'
API_KEY = '6532d6454b8aa370768e63d6ba5a832e'
NUMBER_OF_YEARS = 10
DAYS_PER_REQUEST = 30


def main():
    station_id = sys.argv[1]
    path = 'daily'
    today = datetime.date.today()
    earliest_ordinal = today.toordinal() - (NUMBER_OF_YEARS * 365)
    all_data = []
    for start_ordinal in range(
        earliest_ordinal,
        today.toordinal() + 1,
        DAYS_PER_REQUEST
    ):
        end_ordinal = start_ordinal + (DAYS_PER_REQUEST - 1)
        start_date = datetime.date.fromordinal(start_ordinal)
        end_date = datetime.date.fromordinal(end_ordinal)
        params = {
            'apiKey': API_KEY,
            'format': 'json',
            'units': 'e',
            'numericPrecision': 'decimal',
            'startDate': '{:04}{:02}{:02}'.format(
                start_date.year, start_date.month, start_date.day),
            'endDate': '{:04}{:02}{:02}'.format(
                end_date.year, end_date.month, end_date.day),
            'stationId': station_id,
        }
        print(params['startDate'], params['endDate'])
        url = BASE_URL + path + '?' + urlencode(params)
        response = requests.get(url)
        response.raise_for_status()
        data = response.json()
        all_data.append(data)
        print(data)

    filename = station_id + '.json'
    with open(filename, 'w') as f:
        f.write(json.dumps(all_data))


if __name__ == '__main__':
    main()
